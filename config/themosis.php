<?php

use Sami\Sami;
use Sami\RemoteRepository\GitHubRemoteRepository;
use Sami\Version\GitVersionCollection;
use Symfony\Component\Finder\Finder;

require_once(__DIR__ . '/../app.php');

// START CONFIG
$root_dir = __DIR__ . '/../vendor/themosis';
$src_subdir = '/src';
$name = "themosis";
$package = "themosis/framework";
// END CONFIG

$src_dir = $root_dir . $src_subdir;

$iterator = Finder::create()
    ->files()
    ->name('*.php')
    ->in($src_dir);

$versions = GitVersionCollection::create($src_dir)
    ->add('2.0', '2.0 Branch')
    ->add('1.3', '1.3 Branch')
    ->add('master', 'Master Branch');

$options = [
    'theme'                 => 'default',
    'versions'              => $versions,
    'title'                 => title($name),
    'build_dir'             => buildDir($name),
    'cache_dir'             => cacheDir($name),
    'remote_repository'     => new GitHubRemoteRepository($package, $root_dir),
    'default_opened_level'  => 1,
];

$sami = new Sami($iterator, $options);

return $sami;
