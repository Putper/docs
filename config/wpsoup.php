<?php

use Sami\Sami;
use Sami\RemoteRepository\GitLabRemoteRepository;
use Sami\Version\GitVersionCollection;
use Symfony\Component\Finder\Finder;

require_once(__DIR__ . '/../app.php');

// START CONFIG
$root_dir = __DIR__ . '/../vendor/wp-soup/plugin';
$src_subdir = '/src';
$name = "wp_soup";
$package = "mediasoep/wp-soup/plugin";
// END CONFIG

$src_dir = $root_dir . $src_subdir;

$iterator = Finder::create()
    ->files()
    ->name('*.php')
    ->in($src_dir)
    ->exclude('Core');

$versions = GitVersionCollection::create($src_dir)
    ->add('develop', 'Develop Branch')
    ->add('master', 'Master Branch');

$options = [
    'theme'                 => 'default',
    'versions'              => $versions,
    'title'                 => title($name),
    'build_dir'             => buildDir($name),
    'cache_dir'             => cacheDir($name),
    'remote_repository'     => new GitLabRemoteRepository($package, $root_dir),
    'default_opened_level'  => 1,
];

$sami = new Sami($iterator, $options);

return $sami;
