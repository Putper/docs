<?php

// helper methods

/**
 * create build dir path
 *
 * @param string $name
 * @return string
 */
function buildDir(string $name): string
{
    $name = toDir($name);
    return __DIR__ . "/build/{$name}/%version%";
}

/**
 * create cache dir path
 *
 * @param string $name
 * @return string
 */
function cacheDir(string $name): string
{
    $name = toDir($name);
    return __DIR__ . "/cache/{$name}/%version%";
}

/**
 * create title for name
 *
 * @param string $name
 * @return string
 */
function title(string $name): string
{
    $name = ucfirst($name);
    return "{$name} API Documentation";
}

/**
 * string to dir supported letters only
 *
 * @source https://stackoverflow.com/a/42908256/10984479
 * @param string $name
 *
 * @return string
 */
function toDir(string $name):string
{
    return preg_replace( '/[^a-z0-9]+/', '-', strtolower( $name ) );
}
