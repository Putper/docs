#!/bin/bash

PHP=`which php`
CACHE="./cache/.build"
PUBLIC="public"
BUILD="build"

# remove old files
rm -rf build

# compile sami
for file in config/*.php; do
    $PHP vendor/bin/sami.php update $file
done

# remove old site files
# while read file; do
#     rm $BUILD/$file
# done <$CACHE

# add new site files to cache
# ls $PUBLIC -1 > $CACHE

# copy site files to build
for file in $PUBLIC/*; do
    cp -r $file $BUILD
done
