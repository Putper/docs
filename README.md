# Docs
Generated using [Sami](https://github.com/FriendsOfPHP/Sami).  
API documentation hosted [here](https://docs.jasperv.dev).  
Repository found on Gitlab [here](https://gitlab.com/Mantik/docs).

## Included packages
[themosis/framework](https://github.com/themosis/framework)  
[wp-soup/plugin](https://gitlab.com/mediasoep/wp-soup/plugin)

## Setup
```bash
$ composer install
$ composer build
```
